from django.urls import path

from core.views import Homepage


urlpatterns = [
    path('', Homepage.as_view(), name='homepage')
]
