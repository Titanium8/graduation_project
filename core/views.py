from django.http import JsonResponse
from django.views import View
from random import randint
from django.shortcuts import render


class Homepage(View):
    def get(self, request):
        context = {'title': 'Главная страница'}
        return render(request=request, template_name='core/mainpage.html', context=context)

    def post(self, request):
        if request.method == 'POST':
            domain = request.POST['domain']
            random_number = randint(1, 100)

            if random_number <= 95:
                result = "Домен легитимный"
            else:
                result = "DGA-домен"

            return JsonResponse({'result': result})
